package com.citrineplexus.makandeal.merchant.view;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;

public class SplashView extends AbstractActivity {

	private ImageView logoImg = null;

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(Color.BLACK);
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_view);

		getSupportActionBar().hide();

		logoImg = (ImageView) findViewById(R.id.logo);
		logoImg.postDelayed(Action, 2500L);
	}

	private Runnable Action = new Runnable() {
		public void run() {
			openView(ViewName.SIGN_IN, null);
			SplashView.this.finish();
		}
	};

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

}
