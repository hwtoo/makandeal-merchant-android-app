package com.citrineplexus.makandeal.merchant.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.citrineplexus.MakanDealApplication;
import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;

public class LandingView extends AbstractActivity implements OnClickListener,
		OnDialogCallback {

	private TextView vendorName = null, branchLocation = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing_view);
		getSupportActionBar().setTitle("makandeal");
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setHomeButtonEnabled(false);

		vendorName = (TextView) findViewById(R.id.branchNameId);
		vendorName.setText(MakanDealApplication.getInstance().getVendorName());

		branchLocation = (TextView) findViewById(R.id.branchLocation);
		String branchName = MakanDealApplication.getInstance()
				.getVendorBranchName();
		if (branchName != null && !branchName.equals("")) {
			branchLocation.setText(branchName);
			branchLocation.setVisibility(View.VISIBLE);
		}

		findViewById(R.id.scanCodeButton).setOnClickListener(this);
		findViewById(R.id.enterCodeButton).setOnClickListener(this);
		findViewById(R.id.getHelp).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.scanCodeButton:
			openView(ViewName.SCAN_CODE_VIEW, null);
			break;

		case R.id.enterCodeButton:
			openView(ViewName.ENTER_CODE_VIEW, null);
			break;

		case R.id.getHelp:
			openView(ViewName.HELP_VIEW, null);
			break;

		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if (resultCode == RESULT_OK && data != null && data.getExtras() !=
		// null
		// && data.getExtras().getBoolean("LogOutFlag", false)) {
		// ViewController.getInstance(this)
		// .openView(ViewName.LOGIN_VIEW, null);
		// setResult(RESULT_OK, new Intent());
		// finish();
		// }
	}

	@Override
	public void onDialogClick(int id, DialogName whichDialog, Object result) {

	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	void handlerMessage(Message msg) {

	}

}
