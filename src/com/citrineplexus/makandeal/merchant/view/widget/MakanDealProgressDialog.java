package com.citrineplexus.makandeal.merchant.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.citrineplexus.makandeal.merchant.R;

public class MakanDealProgressDialog extends Dialog {

	public MakanDealProgressDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.loading_view);
		setCanceledOnTouchOutside(false);
	}

}
