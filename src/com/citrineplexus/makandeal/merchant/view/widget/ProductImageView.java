package com.citrineplexus.makandeal.merchant.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ProductImageView extends ImageView {

	public ProductImageView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public ProductImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ProductImageView(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = ((width / 3) * 2); // aspect ration 1.5:1
		setMeasuredDimension(
				MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));

		super.onMeasure(
				MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
	}

}
