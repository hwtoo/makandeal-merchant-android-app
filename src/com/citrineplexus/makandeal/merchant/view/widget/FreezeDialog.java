package com.citrineplexus.makandeal.merchant.view.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.widget.TextView;

import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.view.AbstractActivity.DialogName;

public class FreezeDialog {

	public FreezeDialog(Context context, final OnDialogCallback listener) {
		final ProgressDialog pDialog = new ProgressDialog(context);

		pDialog.setMessage("Please wait 30 seconds to retry.");
		pDialog.setCancelable(false);
		pDialog.show();
		TextView tv1 = (TextView) pDialog.findViewById(android.R.id.message);
		tv1.setTextSize(20);
		tv1.setTextColor(Color.WHITE);
		new CountDownTimer(30 * 1000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				pDialog.setMessage("Please wait " + millisUntilFinished / 1000
						+ " seconds to retry.");
			}

			@Override
			public void onFinish() {
				if (listener != null)
					listener.onDialogClick(6973, DialogName.FREEZE_DIALOG, null);
				pDialog.dismiss();
			}
		}.start();
	}
}
