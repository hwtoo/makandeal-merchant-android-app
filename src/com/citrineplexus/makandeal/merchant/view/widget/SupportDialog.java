package com.citrineplexus.makandeal.merchant.view.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.view.AbstractActivity.DialogName;

public class SupportDialog implements OnClickListener {

	private AlertDialog alertDialog = null;
	private Context context = null;
	private OnDialogCallback listener = null;
	private DialogName dialogName = null;

	public SupportDialog(Context context) {
		this.context = context;
	}

	public void init(DialogName dialogName, String msg, String positiveBtnText,
			String negativeBtnText, OnDialogCallback listener) {
		this.listener = listener;
		this.dialogName = dialogName;
		AlertDialog.Builder mbuidler = new AlertDialog.Builder(context);
		mbuidler.setMessage(msg);

		if (positiveBtnText != null)
			mbuidler.setPositiveButton(positiveBtnText, this);

		if (negativeBtnText != null)
			mbuidler.setNegativeButton(negativeBtnText, this);
		alertDialog = mbuidler.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.setCancelable(false);
		alertDialog.show();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (listener != null)
			listener.onDialogClick(which, dialogName, null);
		alertDialog.dismiss();
	}
}
