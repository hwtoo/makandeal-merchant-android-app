package com.citrineplexus.makandeal.merchant.view.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.util.GlobalService;
import com.citrineplexus.makandeal.merchant.view.AbstractActivity.DialogName;

public class ResetPasswordDialog implements OnClickListener {

	private Context context = null;
	private EditText mEditText = null;
	private AlertDialog alertDialog = null;
	private OnDialogCallback listener = null;
	private DialogName dialogName = null;

	public ResetPasswordDialog(Context context) {
		this.context = context;
	}

	public void init(DialogName dialogName, OnDialogCallback listener) {
		this.listener = listener;
		this.dialogName = dialogName;
		AlertDialog.Builder mbuidler = new AlertDialog.Builder(context);

		mbuidler.setTitle("Reset Password");
		mbuidler.setMessage("Please enter your email address.");

		// Set an EditText view to get user input
		mEditText = new EditText(context);
		mEditText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		mEditText.setHint("Email address");

		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setGravity(Gravity.CENTER_HORIZONTAL);
		layout.setPadding(40, 0, 40, 0);
		layout.addView(mEditText);
		mbuidler.setView(layout);
		mbuidler.setPositiveButton("OK", null);
		mbuidler.setNegativeButton("Cancel", null);

		alertDialog = mbuidler.create();
		alertDialog.show();
		alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
				.setOnClickListener(this);
		alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
				.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case android.R.id.button1:// Button Position
			if (mEditText.getText() != null
					&& mEditText.getText().toString().length() > 0
					&& !mEditText.getText().toString().equals("")) {
				if (GlobalService.isEmailValid(mEditText.getText().toString())) {
					if (listener != null)
						listener.onDialogClick(-1, dialogName, mEditText
								.getText().toString());
					alertDialog.dismiss();
				} else
					Toast.makeText(context,
							"Please enter a valid email address.",
							Toast.LENGTH_SHORT).show();
			} else
				Toast.makeText(context, "Please enter a valid email address.",
						Toast.LENGTH_SHORT).show();
			break;

		case android.R.id.button2:// Button Negative
			alertDialog.dismiss();
			break;

		default:
			break;
		}

	}

}
