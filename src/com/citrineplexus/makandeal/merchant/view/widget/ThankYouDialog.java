package com.citrineplexus.makandeal.merchant.view.widget;

import android.app.AlertDialog;
import android.content.Context;

import com.citrineplexus.makandeal.merchant.R;

public class ThankYouDialog {

	public ThankYouDialog(Context context) {
		openThankYouDialog(context);
	}

	public void openThankYouDialog(Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Thank you");
		builder.setMessage(context.getResources().getString(
				R.string.resetPasswordMsg));
		builder.setPositiveButton("OK", null);

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

}
