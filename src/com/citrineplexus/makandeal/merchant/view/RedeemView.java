package com.citrineplexus.makandeal.merchant.view;

import java.util.ArrayList;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.MakanDealApplication;
import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.util.Calculator;
import com.citrineplexus.makandeal.merchant.util.GlobalService;
import com.citrineplexus.makandeal.merchant.view.widget.ProductImageView;

public class RedeemView extends AbstractActivity implements OnClickListener,
		OnDialogCallback {

	private ProductImageView productImage = null;
	private TextView vendorName = null, productName = null,
			productSubName = null, productPrice = null,
			redemptionLocation = null, packageIncludes = null;
	private LinearLayout packageIncludesLayout = null;
	private BaseEntity entity = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_redeem_view);
		getSupportActionBar().setTitle("Redeem");

		productImage = (ProductImageView) findViewById(R.id.productImage);
		vendorName = (TextView) findViewById(R.id.vendorName);
		productName = (TextView) findViewById(R.id.productName);
		productSubName = (TextView) findViewById(R.id.productSubName);
		productPrice = (TextView) findViewById(R.id.productPrice);
		redemptionLocation = (TextView) findViewById(R.id.redemptionLocation);
		packageIncludesLayout = (LinearLayout) findViewById(R.id.packageIncludesLayout);
		packageIncludes = (TextView) findViewById(R.id.packageInfor);

		findViewById(R.id.redeemButton).setOnClickListener(this);

		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle bundle = getIntent().getExtras();
			entity = BaseEntity.bundleToEntity(bundle);
		}
		initView();
	}

	private void initView() {
		ImageLoader.getInstance(this).DisplayImage(
				entity.get("IMAGE_URL").toString(), productImage,
				ScaleType.CENTER_CROP, false);
		vendorName.setText(entity.get("VENDOR_NAME").toString());
		productName.setText(entity.get("PRODUCT_NAME").toString());
		if (entity.get("PRODUCT_SUB_NAME") != null) {
			productSubName.setText(entity.get("PRODUCT_SUB_NAME").toString());
			productSubName.setVisibility(View.VISIBLE);
		}
		productPrice.setText("RM "
				+ Calculator.round(Double.parseDouble(entity.get("PRICE", "")
						.toString().trim())));
		String vendorAddress = entity.get("STREET", "").toString().trim() + " "
				+ entity.get("ZIP", "").toString().trim() + " "
				+ entity.get("CITY", "").toString().trim();

		redemptionLocation.setText(Html.fromHtml(vendorAddress));
		if (entity.get("PACKAGE_INCLUDES") != null
				&& entity.get("PACKAGE_INCLUDES") instanceof ArrayList<?>) {
			ArrayList<String> benefitsAry = (ArrayList<String>) entity
					.get("PACKAGE_INCLUDES");
			if (!benefitsAry.isEmpty()) {
				packageIncludes.setText(GlobalService
						.convertToBulletText(benefitsAry));
				packageIncludesLayout.setVisibility(View.VISIBLE);
			}
		}

	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));
		String[] dialogMessageAry = new String[3];
		String dialogMessage = "Succefully redeemed.";
		if (entity != null)
			if (entity.get("MESSAGES") != null) {

				BaseEntity errorEntity = (BaseEntity) entity.get("MESSAGES");
				ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) errorEntity
						.get("ERROR");
				String errorMsg = (String) errorAry.get(0).get("MESSAGE");
				if ((errorMsg.toLowerCase()).indexOf("Redeemed".toLowerCase()) != -1)
					dialogMessage = "Redeemed.";
				else if ((errorMsg.toLowerCase()).indexOf("Expired") != -1)
					dialogMessage = "Expired.";
				else if ((errorMsg.toLowerCase()).indexOf("Invalid"
						.toLowerCase()) != -1)
					dialogMessage = "Invalid.";
			}
		dialogMessageAry[0] = dialogMessage;
		dialogMessageAry[1] = "OK";

		openDialog(DialogName.REDEEMED_DIALOG, this, dialogMessageAry);

		// AlertDialog.Builder builder = new AlertDialog.Builder(this,
		// AlertDialog.THEME_HOLO_DARK);
		// View convertView = LayoutInflater.from(this).inflate(
		// R.layout.support_textview, null);
		// TextView textView = (TextView) convertView
		// .findViewById(R.id.textViewid);
		// textView.setText(dialogMessage);
		// builder.setView(convertView);
		// final AlertDialog alertdialog = builder.create();
		// builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		// {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// alertdialog.dismiss();
		// setResult(RESULT_OK, new Intent());
		// finish();
		// }
		// });
		// builder.show();
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.redeemButton) {
			Bundle bundle = new Bundle();
			bundle.putBoolean("LOADING", true);
			bundle.putString("VOUCHER_CODE", entity.get("VOUCHER_CODE")
					.toString());
			bundle.putString("SECURITY_CODE", entity.get("SECURITY_CODE")
					.toString());
			bundle.putString("VENDOR_ID", MakanDealApplication.getInstance()
					.getVendorID());
			getServiceData(1003, 0, bundle, true);
		}
	}

	@Override
	public void onDialogClick(int id, DialogName whichDialog, Object result) {
		if (whichDialog == DialogName.REDEEMED_DIALOG)
			if (id == DialogInterface.BUTTON_POSITIVE)
				finish();
	}

}
