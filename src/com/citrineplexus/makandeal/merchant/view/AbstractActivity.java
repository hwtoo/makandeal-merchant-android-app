package com.citrineplexus.makandeal.merchant.view;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.citrineplexus.MakanDealApplication;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.services.ServiceGateway;
import com.citrineplexus.makandeal.merchant.view.widget.FreezeDialog;
import com.citrineplexus.makandeal.merchant.view.widget.MakanDealProgressDialog;
import com.citrineplexus.makandeal.merchant.view.widget.ResetPasswordDialog;
import com.citrineplexus.makandeal.merchant.view.widget.SupportDialog;
import com.citrineplexus.makandeal.merchant.view.widget.ThankYouDialog;

public abstract class AbstractActivity extends ActionBarActivity implements
		OnDataRequestListener {

	protected Toolbar toolbar;
	private FrameLayout contentLayout;

	private MakanDealProgressDialog mProgressDialog;
	protected boolean loadingOpen = false;
	protected final int LOADING_OPEN = -1011;
	protected final int LOADING_CLOSE = -1012;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.abstract_activity);
		toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
		setSupportActionBar(toolbar);
		contentLayout = (FrameLayout) findViewById(R.id.frameLayout);
		setHomeAsIndicator();
	}

	public void setContentView(int layoutResID) {
		contentLayout.addView(getLayoutInflater().inflate(layoutResID, null));
	}

	private void setHomeAsIndicator() {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {

			if (!isFinishing()) {

				switch (msg.what) {

				case SHOW_EXCEPTION:
					Toast.makeText(AbstractActivity.this,
							getString(R.string.exceptionMsg),
							Toast.LENGTH_SHORT).show();

				case LOADING_CLOSE:
					closeLoading();
					break;
				case LOADING_OPEN:// open progress dialog
					openLoading();
					break;

				}
			}
			handlerMessage(msg);
		}
	};

	abstract void handlerMessage(Message msg);

	private void openLoading() {
		loadingOpen = true;
		if (mProgressDialog != null)
			mProgressDialog = null;
		mProgressDialog = new MakanDealProgressDialog(this);
		mProgressDialog.show();
	}

	private void closeLoading() {
		// if (loadingListener != null)
		// loadingListener
		// .onLoadingStateChange(LoadingListener.LoadingState.CLOSE);
		loadingOpen = false;
		if (mProgressDialog != null)
			mProgressDialog.dismiss();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	protected void getServiceData(int contentID, int position, Bundle params,
			boolean loadingFlag) {
		if (loadingFlag && !loadingOpen) {
			handler.sendEmptyMessage(LOADING_OPEN);
		}

		switch (contentID) {
		default:
			new ServiceGateway().getData(this, contentID, this, position,
					params);
			break;
		}
	}

	protected void verifyVoucher(String voucherCode, String sercurityCode,
			OnDataRequestListener listener) {
		Bundle bundler = new Bundle();
		bundler.putString("VOUCHER_CODE", voucherCode);
		bundler.putString("SECURITY_CODE", sercurityCode);

		String branchId = MakanDealApplication.getInstance()
				.getVendorBranchId();
		if (branchId != null && !branchId.equals("")) {
			Bundle headerBundle = new Bundle();
			headerBundle.putString("branchId", branchId);
			bundler.putBundle("HEADER", headerBundle);
		}

		bundler.putString("VENDOR_ID", MakanDealApplication.getInstance()
				.getVendorID());
		bundler.putBoolean("LOADING", true);
		getServiceData(1002, 0, bundler, true);
	}

	protected int handleRedeemError(Context context, OnDialogCallback callback,
			BaseEntity entity, int invalidCount) {
		boolean branchErrorFlag = false;
		String[] dialogInfor = new String[3];
		BaseEntity errorEntity = (BaseEntity) entity.get("MESSAGES");
		ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) errorEntity
				.get("ERROR");
		String errorMsg = (String) errorAry.get(0).get("MESSAGE");
		if ((errorMsg.toLowerCase()).indexOf("belong".toLowerCase()) != -1) {
			int orErrorCount = errorMsg.indexOf("\" or error");
			dialogInfor[0] = errorMsg.substring("Invalid error \"".length(),
					orErrorCount);
			branchErrorFlag = true;
		} else if ((errorMsg.toLowerCase()).indexOf("Redeemed".toLowerCase()) != -1)
			dialogInfor[0] = "Already Redeemed.";
		else if ((errorMsg.toLowerCase()).indexOf("Expired".toLowerCase()) != -1)
			dialogInfor[0] = "Expired.";
		else
			dialogInfor[0] = "Invalid.";

		if (dialogInfor[0].equalsIgnoreCase("Invalid."))
			invalidCount++;

		if (branchErrorFlag) {
			dialogInfor[1] = "OK";
			dialogInfor[2] = "Get Help";
		} else {
			dialogInfor[1] = "Try Again";
			dialogInfor[2] = "Support";
		}
		openDialog(DialogName.RESULT_DIALOG, callback, dialogInfor);
		return invalidCount;
	}

	protected void handleRedeemSuccess(Context context, BaseEntity entity,
			String voucherText, String securityText) {

		if (entity.get("PRODUCT_NAME") != null
				&& entity.get("BRANCH_NAME") != null) {
			Bundle bundle = entity.getAsBundle();
			bundle.putString("VOUCHER_CODE", voucherText);
			bundle.putString("SECURITY_CODE", securityText);
			openView(ViewName.REDEMPTION_VIEW, bundle);
			finish();
		}
	}

	public void onAquireResult(BaseEntity entity, int position, Bundle params,
			boolean flagToCloseLoading) {
		if (flagToCloseLoading)
			handler.sendEmptyMessage(LOADING_CLOSE);
	}

	protected final int SHOW_EXCEPTION = -1000;

	@Override
	public void onConnectionError(Exception ex) {
		handler.sendEmptyMessage(SHOW_EXCEPTION);
	}

	protected void openView(ViewName viewName, Bundle param) {
		Intent localIntent = null;
		switch (viewName) {
		case SIGN_IN:
			localIntent = new Intent(this, SignInView.class);
			localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			break;

		case LANDING_VIEW:
			localIntent = new Intent(this, LandingView.class);
			break;

		case REGISTER_VIEW:
			localIntent = new Intent(this, RegisterMerchantView.class);
			break;

		case HELP_VIEW:
			localIntent = new Intent(this, HelpView.class);
			break;

		case ENTER_CODE_VIEW:
			localIntent = new Intent(this, EnterCodeView.class);
			break;

		case SCAN_CODE_VIEW:
			localIntent = new Intent(this, ScanCodeView.class);
			break;

		case REDEMPTION_VIEW:
			localIntent = new Intent(this, RedeemView.class);
			break;

		default:
			break;

		}

		if (localIntent != null) {
			if (param != null)
				localIntent.putExtras(param);
			startActivityForResult(localIntent, 1);
		}
	}

	public void openDialog(DialogName dialogName, OnDialogCallback listener,
			String[] dialogInfo) {

		switch (dialogName) {
		case RESET_DIALOG:
			ResetPasswordDialog resetDialog = new ResetPasswordDialog(this);
			resetDialog.init(dialogName, listener);
			break;

		case THANKYOU_DIALOG:
			new ThankYouDialog(this);
			break;

		case SUPPORT_DIALOG:
		case RESULT_DIALOG:
		case REDEEMED_DIALOG:
			SupportDialog dialog = new SupportDialog(this);
			dialog.init(dialogName, dialogInfo[0], dialogInfo[1],
					dialogInfo[2], listener);
			break;

		case FREEZE_DIALOG:
			new FreezeDialog(this, listener);
			break;

		default:
			break;
		}
	}

	public enum ViewName {
		HOME_VIEW, SIGN_IN, LANDING_VIEW, REGISTER_VIEW, HELP_VIEW, ENTER_CODE_VIEW, SCAN_CODE_VIEW, REDEMPTION_VIEW;
	}

	public static enum DialogName {
		SUPPORT_DIALOG, RESULT_DIALOG, CONNECTION_DIALOG, FREEZE_DIALOG, RESET_DIALOG, THANKYOU_DIALOG, REDEEMED_DIALOG;
	}
}
