package com.citrineplexus.makandeal.merchant.view;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.citrineplexus.MakanDealApplication;
import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class SignInView extends AbstractActivity implements OnClickListener,
		OnDialogCallback {

	private EditText emailEditText = null, passwordEditText = null;
	private String email = "", password = "";

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(Color.BLACK);
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signin_view);

		getSupportActionBar().hide();

		emailEditText = (EditText) findViewById(R.id.emailEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);

		findViewById(R.id.playStoreLink).setOnClickListener(this);
		findViewById(R.id.forgotPassword).setOnClickListener(this);
		findViewById(R.id.getHelp).setOnClickListener(this);
		findViewById(R.id.registerMerchnat).setOnClickListener(this);
		findViewById(R.id.signinBtn).setOnClickListener(this);

		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.signInBackground);
		Bitmap bitmap = GlobalService
				.fastblur(BitmapFactory.decodeResource(getResources(),
						R.drawable.splash), 10);
		Drawable dr = new BitmapDrawable(bitmap);
		relativeLayout.setBackground(dr);

	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.playStoreLink:
			GlobalService.openPlayStore(this);
			break;

		case R.id.getHelp:
			openDialog(DialogName.SUPPORT_DIALOG, this, new String[] {
					getString(R.string.supportText), "OK", "Cancel" });
			break;

		case R.id.forgotPassword:
			openDialog(DialogName.RESET_DIALOG, this, null);
			break;

		case R.id.signinBtn:
			try {
				email = emailEditText.getText().toString();
				password = passwordEditText.getText().toString();
				if (GlobalService.isEmailValid(email) && !password.equals("")
						&& password.length() > 0) {
					Bundle mbundle = new Bundle();
					mbundle.putString("EMAIL", email);
					mbundle.putBoolean("LOADING", true);
					getServiceData(1000, 0, mbundle, true);
				} else
					Toast.makeText(this, "Username or password is incorrect.",
							Toast.LENGTH_SHORT).show();

			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.registerMerchnat:
			Bundle bundle = new Bundle();
			bundle.putString("STAGE", "REGISTER");
			openView(ViewName.REGISTER_VIEW, bundle);
			break;

		default:
			break;
		}

	}

	@Override
	public void onDialogClick(int id, DialogName whichDialog, Object result) {
		if (id == DialogInterface.BUTTON_POSITIVE)
			if (result instanceof String) {
				Bundle params = new Bundle();
				params.putString("EMAIL", (String) result);
				getServiceData(1004, 0, params, false);

			} else if (whichDialog == DialogName.SUPPORT_DIALOG) {
				GlobalService.callSupportIntent(this,
						getString(R.string.customerServiceNumber));
			}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));
		if (position == 1000) {
			if (entity != null) {
				String password = entity.get("SALT") + this.password;
				Bundle bundle = new Bundle();

				Bundle headerBundle = new Bundle();
				headerBundle.putString("LoginInfo", this.email + ":"
						+ GlobalService.generateMD5(password));
				bundle.putBundle("HEADER", headerBundle);
				bundle.putString("VENDOR_ID", (String) entity.get("SALT"));
				bundle.putBoolean("LOADING", true);
				getServiceData(1001, 0, bundle, true);
			}
		} else if (position == 1001) {
			if (entity != null) {
				if (entity.get("MESSAGES") != null) {
					BaseEntity baseEntity = (BaseEntity) entity.get("MESSAGES");
					ArrayList<BaseEntity> ary = (ArrayList<BaseEntity>) baseEntity
							.get("ERROR");
					if (ary != null) {
						Toast.makeText(this,
								(String) ary.get(0).get("MESSAGE"),
								Toast.LENGTH_SHORT).show();
					}
				} else {
					MakanDealApplication.getInstance().setValueFromServer(
							entity, this.email);
					MakanDealApplication.getInstance().storeValue();
					openView(ViewName.LANDING_VIEW, null);
					finish();
				}
			}
		} else if (position == 1004) {
			openDialog(DialogName.THANKYOU_DIALOG, null, null);
		}
	}
}
