package com.citrineplexus.makandeal.merchant.view;

import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.citrineplexus.MakanDealApplication;
import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class HelpView extends AbstractActivity implements OnClickListener,
		OnDialogCallback {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Help");
		setContentView(R.layout.activity_help_view);

		TextView vendorName = (TextView) findViewById(R.id.branchNameId);
		vendorName.setText(MakanDealApplication.getInstance().getVendorName());

		TextView vendorEmail = (TextView) findViewById(R.id.branchEmail);
		vendorEmail
				.setText(MakanDealApplication.getInstance().getVendorEmail());

		findViewById(R.id.contactEmail).setOnClickListener(this);
		findViewById(R.id.contactNumber).setOnClickListener(this);
		findViewById(R.id.signOut).setOnClickListener(this);
		
		TextView appVersion = (TextView)findViewById(R.id.appVersion);
		
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			appVersion.setText("App Version " + pInfo.versionName);
			String appurl = getResources().getString(R.string.apiUrl);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.contactEmail:
			GlobalService.emailSupportIntent(this);
			break;

		case R.id.contactNumber:
			openDialog(DialogName.SUPPORT_DIALOG, this, new String[] {
					getString(R.string.supportText), "OK", "Cancel" });
			break;

		case R.id.signOut:
			MakanDealApplication.getInstance().removeAllValue();
			openView(ViewName.SIGN_IN, null);
			finish();
			break;

		default:
			break;
		}

	}

	@Override
	public void onDialogClick(int id, DialogName whichDialog, Object result) {
		if (id == DialogInterface.BUTTON_POSITIVE)
			GlobalService.callSupportIntent(this,
					getString(R.string.customerServiceNumber));
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	void handlerMessage(Message msg) {

	}

}
