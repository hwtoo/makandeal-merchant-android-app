package com.citrineplexus.makandeal.merchant.view;

import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;

public class EnterCodeView extends AbstractActivity implements OnClickListener,
		OnDialogCallback {

	private EditText voucherCodeField = null, securityCodeField = null;
	private int invalidCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enter_code_view);
		getSupportActionBar().setTitle("Enter Code");

		voucherCodeField = (EditText) findViewById(R.id.voucherCodeField);
		securityCodeField = (EditText) findViewById(R.id.securityCodeField);

		findViewById(R.id.verifyBtn).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.enter_code_menu, menu);
		MenuItem playMenu = menu.findItem(R.id.enterCode);
		playMenu.setIcon(getResources().getDrawable(R.drawable.icon_scan_code));
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.enterCode:
			openView(ViewName.SCAN_CODE_VIEW, null);
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));
		if (entity != null) {
			if (entity.get("MESSAGES") != null) {
				invalidCount = handleRedeemError(this, this, entity,
						invalidCount);
			} else {
				String voucherText = voucherCodeField.getText().toString();
				String securityText = securityCodeField.getText().toString();
				handleRedeemSuccess(this, entity, voucherText, securityText);
			}
		}
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.verifyBtn)
			verifyVoucher(voucherCodeField.getText().toString(),
					securityCodeField.getText().toString(), this);

	}

	@Override
	public void onDialogClick(int id, DialogName whichDialog, Object result) {

	}

}
