package com.citrineplexus.makandeal.merchant.view;

import android.os.Bundle;
import android.os.Message;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class RegisterMerchantView extends AbstractActivity {

	private WebView webView = null;
	private Bundle localBundle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Sign Up");
		setContentView(R.layout.activity_register_merchant_view);

		webView = (WebView) findViewById(R.id.webView);
		webView.setWebViewClient(new WebViewClient());
		webView.clearCache(true);
		webView.getSettings().setJavaScriptEnabled(true);

		localBundle = getIntent().getExtras();
		if (localBundle != null)
			if (localBundle.getString("STAGE", "").equals("REGISTER"))
				webView.loadUrl(GlobalService.getRegisterUrl()
						+ "/app-partner-with-us");

	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	void handlerMessage(Message msg) {

	}

}
