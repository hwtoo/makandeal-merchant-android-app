package com.citrineplexus.makandeal.merchant.view;

import ozurafirstlogix.qrcodelibrary.QRCodeSystem;
import ozurafirstlogix.qrcodelibrary.QRCodeSystem.QRCodeInterface;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.callback.OnDialogCallback;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class ScanCodeView extends AbstractActivity implements OnClickListener,
		QRCodeInterface, OnDialogCallback {

	private FrameLayout mCaremaFrame = null;
	private ImageView scanArea = null, flashSwitch = null;
	private String[] resultCode;
	private int invalidCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_code_view);
		getSupportActionBar().setTitle("Scan Code");
		getSupportActionBar().setDisplayShowHomeEnabled(false);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mCaremaFrame = (FrameLayout) findViewById(R.id.cameraFrame);
		scanArea = (ImageView) findViewById(R.id.focusArea);
		flashSwitch = (ImageView) findViewById(R.id.flashSwitch);
		flashSwitch.setOnClickListener(this);
		QRCodeSystem.getInstance().initCamera();
	}

	@Override
	protected void onPause() {
		super.onPause();
		QRCodeSystem.getInstance().releaseCamera();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {

			int array[] = new int[2];
			scanArea.getLocationOnScreen(array);
			QRCodeSystem.getInstance().initQRCodeSystem(
					this,
					mCaremaFrame,
					scanArea,
					((WindowManager) getSystemService(WINDOW_SERVICE))
							.getDefaultDisplay(), getStatusBarHeight(), this);
//			QRCodeSystem.getInstance().rescan();
		}
	}

	public int getStatusBarHeight() {
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height",
				"dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.enter_code_menu, menu);
		MenuItem switchMenu = menu.findItem(R.id.enterCode);
		switchMenu.setIcon(getResources().getDrawable(
				R.drawable.icon_enter_code));
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.enterCode:
			openView(ViewName.ENTER_CODE_VIEW, null);
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));
		if (entity != null) {
			if (entity.get("MESSAGES") != null) {
				invalidCount = handleRedeemError(this, this, entity,
						invalidCount);
			} else {
				handleRedeemSuccess(this, entity, resultCode[0], resultCode[1]);
			}
		}
	}

	@Override
	void handlerMessage(Message msg) {
		if (msg.what == -100) {
			if (QRCodeSystem.getInstance().isFlashOpen())
				flashSwitch.setImageResource(R.drawable.icon_flash_on);
			else
				flashSwitch.setImageResource(R.drawable.icon_flash_off);
		}
//		QRCodeSystem.getInstance().rescan();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.flashSwitch) {
			if (getPackageManager().hasSystemFeature(
					PackageManager.FEATURE_CAMERA_FLASH)) {
				QRCodeSystem.getInstance().enableFlash();
			}
			handler.sendEmptyMessage(-100);
		}
	}

	@Override
	public void QRCodeResult(String result) {
		if (result.indexOf("|") == -1) {
			invalidCount++;
			String[] dialogInfor = new String[] { "Invalid.", "Try Again",
					"Support" };
			openDialog(DialogName.RESULT_DIALOG, this, dialogInfor);
		} else {
			resultCode = result.split("\\|");
			verifyVoucher(resultCode[0], resultCode[1], this);
		}
	}

	@Override
	public void onDialogClick(int id, DialogName whichDialog, Object result) {
		if (whichDialog == DialogName.RESULT_DIALOG) {
			if (id == DialogInterface.BUTTON_POSITIVE) {
				if (invalidCount > 3) {
					openDialog(DialogName.FREEZE_DIALOG, this, null);
					invalidCount = 0;
				} else
					QRCodeSystem.getInstance().rescan();
				// handler.sendEmptyMessage(0);
			} else if (id == DialogInterface.BUTTON_NEGATIVE)
				openDialog(DialogName.SUPPORT_DIALOG, this, new String[] {
						getString(R.string.supportText), "OK", "Cancel" });
		} else if (whichDialog == DialogName.SUPPORT_DIALOG) {
			if (id == DialogInterface.BUTTON_POSITIVE)
				GlobalService.callSupportIntent(this,
						getResources()
								.getString(R.string.customerServiceNumber));
		} else if (whichDialog == DialogName.FREEZE_DIALOG) {
			QRCodeSystem.getInstance().rescan();
		}
	}

	// private void handlerResultDialogCallback(int id) {
	// if (id == DialogInterface.BUTTON_POSITIVE) {
	// if (invalidCount > 3) {
	// openDialog(
	// DialogName.FREEZE_DIALOG, null, null);
	// invalidCount = 0;
	// }
	// } else if (id == DialogInterface.BUTTON_NEGATIVE) {
	// ViewController.getInstance(this).openDialog(
	// DialogName.SUPPORT_DIALOG,
	// this,
	// new String[] { getString(R.string.supportText), "OK",
	// "Cancel" });
	// }
	// }

}
