package com.citrineplexus.makandeal.merchant.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.citrineplexus.makandeal.merchant.entity.BaseEntity;

public class JsonParser {
	private static String modifiedKey(String key) {
		return key.toUpperCase();
	}

	public static synchronized BaseEntity parseJSONtoEntity(String jsonData,
			BaseEntity entity) {
		if (jsonData == null || jsonData.length() <= 0) 
			return null;

		BaseEntity localEntity = null;
		if (entity != null)
			localEntity = entity;
		else {
			localEntity = new BaseEntity();
		}
		JSONObject object = null;
		try {
			object = new JSONObject(jsonData);
		} catch (JSONException localJSONException) {
			try {
				object = new JSONObject("{\"JSONARRAY\":" + jsonData + "}");
			} catch (Exception e) {
				return null;
			}
		}
		Iterator<?> keys = object.keys();

		while (keys.hasNext()) {
			try {

				String key = (String) keys.next();
				if (object.get(key) instanceof String) {
					localEntity.put(modifiedKey(key), object.getString(key));
				} else if (object.get(key) instanceof Integer) {
					localEntity.put(modifiedKey(key),
							Integer.valueOf(object.getInt(key)));
				} else if (object.get(key) instanceof Boolean) {
					localEntity.put(modifiedKey(key),
							Boolean.valueOf(object.getBoolean(key)));
				} else if (object.get(key) instanceof Double) {
					localEntity.put(modifiedKey(key),
							Double.valueOf(object.getDouble(key)));
				} else if (object.get(key) instanceof Long) {
					localEntity.put(modifiedKey(key),
							Long.valueOf(object.getLong(key)));
				} else if (object.get(key) instanceof JSONObject) {
					localEntity.put(
							modifiedKey(key),
							parseJSONtoEntity(object.getJSONObject(key)
									.toString(), null));
				} else if (object.get(key) instanceof JSONArray) {
					JSONArray jsonArray = object.getJSONArray(key);
					ArrayList<Object> arrayEntity = new ArrayList<Object>();
					for (int i = 0; i < jsonArray.length(); i++) {
						if (jsonArray.get(i) instanceof JSONObject
								|| jsonArray.get(i) instanceof JSONArray) {
							arrayEntity.add(parseJSONtoEntity(jsonArray.get(i)
									.toString(), null));
						} else {
							arrayEntity.add(jsonArray.get(i));
						}
					}
					localEntity.put(modifiedKey(key), arrayEntity);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return localEntity;
	}

	public static JSONObject convertEntityToJSON(BaseEntity entity) {
		JSONObject jsonObj = new JSONObject();
		try {
			for (String key : entity.getKeySet()) {
				if (entity.get(key) instanceof ArrayList<?>) {
					JSONArray jsonAry = new JSONArray();
					for (int i = 0; i < ((ArrayList<?>) entity.get(key)).size(); i++) {
						if (((ArrayList<?>) entity.get(key)).get(i) instanceof BaseEntity) {
							jsonAry.put(convertEntityToJSON((BaseEntity) ((ArrayList<?>) entity
									.get(key)).get(i)));
						} else
							jsonAry.put(((ArrayList<?>) entity.get(key)).get(i));
					}
					// for (BaseEntity en : (ArrayList<BaseEntity>) entity
					// .get(key))
					// jsonAry.put(convertEntityToJSON(en));
					jsonObj.put(key, jsonAry);

				} else
					jsonObj.put(key, entity.get(key));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
}
