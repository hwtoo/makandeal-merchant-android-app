package com.citrineplexus.makandeal.merchant.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Calculator {
	public static float rcound(float paramFloat, int paramInt) {
		return new BigDecimal(Float.toString(paramFloat)).setScale(paramInt, 4)
				.floatValue();
	}

	public static String round(double paramDouble) {
		return new DecimalFormat("#,###,##0.00").format(paramDouble);
	}
}
