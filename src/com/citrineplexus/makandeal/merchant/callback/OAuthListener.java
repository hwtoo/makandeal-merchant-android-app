package com.citrineplexus.makandeal.merchant.callback;

import android.os.Bundle;

public interface OAuthListener {
	public void returnMessage(int id, Object obj);

	public void oauthReady(boolean status, Bundle extra);

	public void onAcquireProfile();
}
