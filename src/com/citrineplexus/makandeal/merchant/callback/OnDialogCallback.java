package com.citrineplexus.makandeal.merchant.callback;

import com.citrineplexus.makandeal.merchant.view.AbstractActivity.DialogName;


public interface OnDialogCallback {

	public void onDialogClick(int id, DialogName whichDialog, Object result);
}
