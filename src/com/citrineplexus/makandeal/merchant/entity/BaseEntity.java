package com.citrineplexus.makandeal.merchant.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import android.os.Bundle;

public class BaseEntity {
	private final String[] sortListRule = { "A", "E", "R", "L", "S" };
	private HashMap<String, Object> entityValue = null;
	private BaseEntityListener listener = null;
	private Integer statusId = -1, region = -1;
	private boolean expiredSoon = false;
	private Date validDate = null;
	private boolean couponCheckBox = false;

	public void couponCheckBoxFlag() {
		couponCheckBox = !couponCheckBox;
	}

	public boolean getCouponCheckBox() {
		return this.couponCheckBox;
	}

	public BaseEntity() {
		entityValue = new HashMap<String, Object>();
	}

	public BaseEntity(BaseEntityListener lis) {
		listener = lis;
		entityValue = new HashMap<String, Object>();
	}

	public void clearData() {
		this.entityValue.clear();
	}

	public boolean match(String key, Object value) {
		return (entityValue.get(key) != null)
				&& (entityValue.get(key).equals(value));
	}

	public boolean remove(String key) {
		if (get(key) != null) {
			entityValue.remove(key);
			return true;
		}
		return false;
	}

	public Object get(String key) {
		return entityValue.get(key);
	}

	public Object get(String key, Object defaultValue) {
		if (entityValue.get(key) == null)
			return defaultValue;
		return entityValue.get(key);
	}

	public Set<String> getKeySet() {
		return entityValue.keySet();
	}

	public void put(String key, Object value) {
		if (key.equals("STATUS")) {
			if (value instanceof String)
				setStatus((String) value);
		}
		if (key.equals("VALID_TO")) {
			if (value instanceof String)
				checkExpireSoon((String) value);
		}

		if (key.equals("REGION_ID")) {
			if (value instanceof String)
				region = Integer.valueOf(((String) value));
		}

		entityValue.put(key, value);
		if (listener != null)
			listener.onChange(this, key);
	}

	private void setStatus(String value) {
		for (int i = 0; i < sortListRule.length; i++) {
			if (value.equals(sortListRule[i])) {
				this.statusId = i;
				break;
			}
		}
	}

	public boolean getExpireSoonFlag() {
		return this.expiredSoon;
	}

	private void checkExpireSoon(String value) {
		try {
			Calendar expDate = Calendar.getInstance();
			Date mDate = ((DateFormat) new SimpleDateFormat("yyyy-MM-dd"))
					.parse(value);
			expDate.setTime(mDate);
			validDate = mDate;
			expDate.add(Calendar.MONTH, -1);
			if (Calendar.getInstance().after(expDate))
				expiredSoon = true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void setListener(BaseEntityListener lis) {
		listener = lis;
	}

	public BaseEntity merge(BaseEntity src) {
		for (String key : src.getKeySet()) {
			put(key, src.get(key));
		}
		return this;
	}

	/***
	 * @exception it
	 *                will only return current level and primitive value only
	 * @return
	 */
	public Bundle getAsBundle() {
		Bundle entityBundle = new Bundle();
		for (String key : this.getKeySet())
			if (this.get(key) instanceof String)
				entityBundle.putString(key, this.get(key).toString());
			else if (this.get(key) instanceof Boolean)
				entityBundle.putBoolean(key, (Boolean) this.get(key));
			else if (this.get(key) instanceof Integer)
				entityBundle.putInt(key, (Integer) this.get(key));
			else if (this.get(key) instanceof Double)
				entityBundle.putDouble(key, (Double) this.get(key));
			else if (this.get(key) instanceof ArrayList<?>)
				entityBundle.putStringArrayList(key,
						(ArrayList<String>) this.get(key));
		return entityBundle;
	}

	public static BaseEntity bundleToEntity(Bundle bundle) {
		BaseEntity entity = new BaseEntity();
		for (String key : bundle.keySet())
			if (bundle.get(key) instanceof String)
				entity.put(key, bundle.getString(key));
			else if (bundle.get(key) instanceof Boolean)
				entity.put(key, bundle.getBoolean(key));
			else if (bundle.get(key) instanceof Integer)
				entity.put(key, bundle.getInt(key));
			else if (bundle.get(key) instanceof Double)
				entity.put(key, bundle.getDouble(key));
			else if (bundle.get(key) instanceof ArrayList<?>)
				entity.put(key, bundle.getStringArrayList(key));
		return entity;
	}

	public static abstract interface BaseEntityListener {
		public abstract void onChange(BaseEntity entity, String key);
	}

	public static Comparator<BaseEntity> COMPARE_BY_STATUS = new Comparator<BaseEntity>() {
		public int compare(BaseEntity one, BaseEntity other) {
			return one.statusId.compareTo(other.statusId);
		}
	};

	public static Comparator<BaseEntity> COMPARE_BY_VALID_DATE = new Comparator<BaseEntity>() {
		public int compare(BaseEntity one, BaseEntity other) {
			if (one.validDate == null || other.validDate == null)
				return 0;
			return other.validDate.compareTo(one.validDate);
		}
	};

	public static Comparator<BaseEntity> COMPARE_BY_REGION = new Comparator<BaseEntity>() {
		public int compare(BaseEntity one, BaseEntity other) {
			return one.region.compareTo(other.region);
		}
	};

}
