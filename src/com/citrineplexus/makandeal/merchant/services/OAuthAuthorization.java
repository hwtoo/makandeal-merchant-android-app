package com.citrineplexus.makandeal.merchant.services;

import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;

import org.apache.http.client.methods.HttpUriRequest;

import android.content.Context;
import android.os.Bundle;

import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.merchant.callback.OAuthListener;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class OAuthAuthorization implements OnDataRequestListener, OAuthListener {
	private String ACCESS_TOKEN_URL = "";
	private String AUTH_URL = "";
	private String REQUEST_URL = "";
	private final String CALLBACK_URL = "http://makandeal.callback";
	private final String CONSUMER_KEY = "0pesoheclzna15ffnnh6otvrm735xajg";
	private final String CONSUMER_SECRET = "pd6xahzraqgjxka3k6u7ifuppdmko5x0";

	// For Staging
	private final String LOGIN_TOKEN_KEY_STAGING = "zbxps8usi95uaajoxv6syzkutqz1bekc";
	private final String LOGIN_TOKEN_SECRET_STAGING = "dj0b5pfrrdntvt6kth0y2ejckqhgoet5";
	// For Live
	private final String LOGIN_TOKEN_KEY = "r80wsh6gdejgq86wmdvq26fxw5xmr6bh";
	private final String LOGIN_TOKEN_SECRET = "os2m26kbgnuqnt2vdrd6lcc08ielupxj";

	private CommonsHttpOAuthConsumer consumer = null;
	private CommonsHttpOAuthProvider provider = null;
	private static OAuthAuthorization instance = null;
	private boolean isLogin = false;
	private BaseEntity userProfile = null;
	private OAuthListener listener = null;
	private int retryCounter = 0;
	private Context context = null;

	private OAuthAuthorization() {
		ACCESS_TOKEN_URL = GlobalService.getApiUrl() + "/oauth/token";
		AUTH_URL = GlobalService.getApiUrl() + "/oauth/authorize";
		REQUEST_URL = GlobalService.getApiUrl() + "/oauth/initiate";
	}

	public static OAuthAuthorization getInstance() {
		return instance = instance == null ? new OAuthAuthorization()
				: instance;
	}

	public void Login(final OAuthListener lis) throws Exception {
		if (isLogin)
			throw new Exception("User already login");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY,
							CONSUMER_SECRET);
					if (GlobalService.isLive())
						consumer.setTokenWithSecret(LOGIN_TOKEN_KEY,
								LOGIN_TOKEN_SECRET);
					else
						consumer.setTokenWithSecret(LOGIN_TOKEN_KEY_STAGING,
								LOGIN_TOKEN_SECRET_STAGING);
					lis.oauthReady(true, null);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public void signRequest(HttpUriRequest request) {
		try {
			// if (!isLogin)
			// throw new Exception("User not login, unable to sign: "
			// + request.getURI());
			if (consumer == null)
				try {
					consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY,
							CONSUMER_SECRET);
					if (GlobalService.isLive())
						consumer.setTokenWithSecret(LOGIN_TOKEN_KEY,
								LOGIN_TOKEN_SECRET);
					else
						consumer.setTokenWithSecret(LOGIN_TOKEN_KEY_STAGING,
								LOGIN_TOKEN_SECRET_STAGING);
					// lis.oauthReady(true, null);

				} catch (Exception e) {
					e.printStackTrace();
				}

			consumer.sign(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getAccessTokenWithVerifier(final String verifier,
			final OAuthListener lis) throws Exception {
		provider = new CommonsHttpOAuthProvider(REQUEST_URL, ACCESS_TOKEN_URL,
				AUTH_URL);
		provider.setOAuth10a(true);
		new Thread(new Runnable() {
			public void run() {
				try {
					boolean successGetAccessToken = provider
							.retrieveAccessToken(consumer, verifier);

					isLogin = successGetAccessToken;

					Bundle extras = null;
					if (successGetAccessToken) {
						extras = new Bundle();
						extras.putString("TOKEN", provider.getAccessToken());
						extras.putString("SECRET", provider.getAccessSecret());
					}
					lis.oauthReady(successGetAccessToken, extras);
				} catch (Exception localException) {
					localException.printStackTrace();
				}
			}
		}).start();
	}

	// public void logoutUser(Context context) {
	// isLogin = false;
	// if (userProfile != null)
	// userProfile.clearData();
	// userProfile = null;
	// PersistentData.getInstance().getPreferences(context).edit()
	// .remove("TOKEN").commit();
	// PersistentData.getInstance().getPreferences(context).edit()
	// .remove("SECRET").commit();
	// }

	// public boolean isUserLogin() {
	// return isLogin;
	// }

	public String getCallbackUrl() {
		return CALLBACK_URL;
	}

	public void assignUserProfile(BaseEntity profile) {
		userProfile = profile;
	}

	public BaseEntity getUserProfileData() {
		return userProfile;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		// if (params.getBoolean("USER_INFO", false)) {
		// if (OAuthAuthorization.getInstance().getUserProfileData() == null) {
		// if (entity == null) {
		// if (retryCounter++ < 5) {
		// Bundle localBundle = new Bundle();
		// localBundle.putInt("CONTENT_ID", 1015);
		// try {
		// new ServiceGateway().getUserInfo(context, this,
		// localBundle);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// }
		//
		// if (listener != null) {
		// listener.onAcquireProfile();
		// listener = null;
		// }
		// context = null;
		// return;
		// }
		// retryCounter = 0;
		//
		// BaseEntity userEn = null;
		// for (String key : entity.getKeySet()) {
		// userEn = (BaseEntity) entity.get(key);
		// userEn.put("ID", key);
		// }
		// OAuthAuthorization.getInstance().assignUserProfile(userEn);
		// }
		//
		// if (listener != null) {
		// listener.onAcquireProfile();
		// listener = null;
		// }
		// context = null;
		// }
	}

	// public void getUserProfile(Context context, OAuthListener lis) {
	// if (!OAuthAuthorization.getInstance().isUserLogin()
	// && OAuthAuthorization.getInstance().getUserProfileData() != null) {
	// if (lis != null)
	// lis.onAcquireProfile();
	// return;
	// }
	//
	// if (context == null)
	// return;
	//
	// this.context = context;
	// listener = lis;
	//
	// Bundle localBundle = new Bundle();
	// localBundle.putInt("CONTENT_ID", 1015);
	// localBundle.putBoolean("USER_INFO", true);
	// try {
	// new ServiceGateway().getUserInfo(context, this, localBundle);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	@Override
	public void returnMessage(int id, Object obj) {

	}

	@Override
	public void oauthReady(boolean status, Bundle extra) {

	}

	@Override
	public void onAcquireProfile() {
	}

	@Override
	public void onConnectionError(Exception ex) {

	}

}
