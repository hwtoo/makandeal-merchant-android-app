package com.citrineplexus.makandeal.merchant.services;

import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;

import com.citrineplexus.library.webservice.AbstractServiceGateway;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class ServiceGateway extends AbstractServiceGateway {

	public ServiceGateway() {
	}

	private String getAPIEntry(Context con) {
		return GlobalService.getApiUrl();
	}

	public void getData(Context context, int contentID,
			OnDataRequestListener listener, int position, Bundle params) {
		this.context = context;
		if (params == null)
			params = new Bundle();
		params.putInt("CONTENT_ID", contentID);

		if (!isNetworkConnected(context)) {
			if (listener != null)
				listener.onConnectionError(null);
			return;
		}

		switch (contentID) {

		case 1000:
			getVendorSalt(context, listener, params);
			break;

		case 1001:
			merchantLogin(context, listener, params);
			break;

		case 1002:
			verifyVoucher(context, listener, params);
			break;

		case 1003:
			updateServiceVoucherRedeem(context, listener, params);
			break;

		case 1004:
			forgotPassword(context, listener, params);
			return;

		}
	}

	private void getVendorSalt(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		this.context = context;
		if (extraParam.get("EMAIL") == null)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/vendorbranchsalt/"
						+ extraParam.getString("EMAIL", ""),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void merchantLogin(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		// if (extraParam.get("EMAIL") == null
		// || extraParam.get("PASSWORD") == null)
		// return;
		// try {
		// JSONObject obj = new JSONObject();
		// obj.put("email", extraParam.getString("EMAIL"));
		// obj.put("hash", extraParam.getString("PASSWORD"));
		// extraParam.putString("POST_ENTITY", obj.toString());
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context)
						+ "/api/rest/merchantlogin/vendorbranchlogin",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void updateServiceVoucherRedeem(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		this.context = context;
		if (extraParam.get("VOUCHER_CODE") == null
				|| extraParam.get("SECURITY_CODE") == null
				|| extraParam.get("VENDOR_ID") == null)
			return;

		try {
			JSONObject obj = new JSONObject();
			obj.put("code", extraParam.getString("VOUCHER_CODE"));
			obj.put("pin", extraParam.getString("SECURITY_CODE"));
			obj.put("vendor_id", extraParam.getString("VENDOR_ID"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.PUT, listener,
				getAPIEntry(context) + "/api/rest/svverifier",
				extraParam.getInt("CONTENT_ID"), extraParam });

	}

	private void forgotPassword(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam.get("EMAIL") == null)
			return;
		try {
			JSONObject obj = new JSONObject();
			obj.put("email", extraParam.getString("EMAIL"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/passwordPost",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void userRegister(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam.get("EMAIL") == null
				|| extraParam.get("PASSWORD") == null
				|| extraParam.get("FIRSTNAME") == null
				|| extraParam.get("LASTNAME") == null)
			return;

		try {
			JSONObject obj = new JSONObject();
			obj.put("email", extraParam.getString("EMAIL"));
			obj.put("encrypted_password", Base64.encodeToString(extraParam
					.getString("PASSWORD").getBytes("UTF-8"), Base64.DEFAULT));
			obj.put("firstname", extraParam.getString("FIRSTNAME"));
			obj.put("lastname", extraParam.getString("LASTNAME"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/customers",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void verifyVoucher(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		this.context = context;
		if (extraParam.get("VOUCHER_CODE") == null
				|| extraParam.get("SECURITY_CODE") == null
				|| extraParam.get("VENDOR_ID") == null)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/svverifier/"
						+ extraParam.getString("VOUCHER_CODE") + "/"
						+ extraParam.getString("SECURITY_CODE") + "/"
						+ extraParam.getString("VENDOR_ID")
						+ "?imgSize=450x300", extraParam.getInt("CONTENT_ID"),
				extraParam });
	}

}
