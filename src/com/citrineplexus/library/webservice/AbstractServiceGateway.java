package com.citrineplexus.library.webservice;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.services.OAuthAuthorization;
import com.citrineplexus.makandeal.merchant.util.JsonParser;

public abstract class AbstractServiceGateway {

	public Bundle headerBundle = null;
	public Context context = null;

	private String executeDelete(String url, Bundle extra)
			throws ClientProtocolException, IOException {
		return executeRequest(new HttpDelete(url), extra);
	}

	private String executeGet(String url, Bundle extra)
			throws ClientProtocolException, IOException {
		return executeRequest(new HttpGet(url), extra);
	}

	private String executePut(String url, String stringEntity, Bundle extra)
			throws ClientProtocolException, IOException {
		HttpPut localHttpPut = new HttpPut(url);
		localHttpPut.setEntity(new StringEntity(stringEntity));
		return executeRequest(localHttpPut, extra);
	}

	private String executePost(String url, String stringEntity, Bundle extra)
			throws ClientProtocolException, IOException {
		HttpPost localHttpPost = new HttpPost(url);
		localHttpPost.setEntity(new StringEntity(stringEntity));
		return executeRequest(localHttpPost, extra);
	}

	private String executeRequest(HttpUriRequest request, Bundle extra)
			throws ClientProtocolException, IOException {
		DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
		request.setHeader("Content-Type", "application/json");
		request.setHeader("Accept", "application/json");

		if (extra != null && extra.get("HEADER_KEYNAME") != null)
			request.setHeader("access-token",
					(String) extra.get("HEADER_KEYNAME"));

		if (extra != null && extra.get("HEADER") != null) {
			Bundle headerBundle = extra.getBundle("HEADER");
			for (String headerKey : headerBundle.keySet())
				request.setHeader(headerKey, headerBundle.getString(headerKey));
		}

		String str = "";

		// if (extra.getBoolean("SIGN_ADMIN", false)) {
		// OAuthAuthorization.getInstance().signLoginRequest(request);
		// } else if (OAuthAuthorization.getInstance().isUserLogin()) {
		// OAuthAuthorization.getInstance().signRequest(request);
		// }

		// if (OAuthAuthorization.getInstance().isUserLogin()) {
		// Log.d("Bello2", "OAuth sign request api");
		OAuthAuthorization.getInstance().signRequest(request);
		// }

		// System.out.println("VCVCVCVCV URI:" + request.getURI());
		// for (Header head : request.getAllHeaders())
		// if (head.getName().equals("Authorization"))
		// System.out.println("VCVCVC name:" + head.getName() + " value:"
		// + head.getValue());
		str = (String) localDefaultHttpClient.execute(request,
				new HttpResponseHandler());

		localDefaultHttpClient.getConnectionManager().shutdown();
		return str;
	}

	private class HttpResponseHandler implements ResponseHandler<String> {

		public HttpResponseHandler() {
			super();
		}

		@Override
		public String handleResponse(HttpResponse httpResponse)
				throws IOException {
			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				if (httpResponse.getStatusLine().getStatusCode() == 401) {
					// OAuthAuthorization.getInstance().logoutUser(context);
				}
				// throw new IOException("statusCode: "
				// + httpResponse.getStatusLine().getStatusCode());
			}
			HttpEntity localHttpEntity = httpResponse.getEntity();
			String result = "";
			// long totalLength = localHttpEntity.getContentLength();
			InputStream localInputStream = localHttpEntity.getContent();

			headerBundle = new Bundle();
			for (org.apache.http.Header header : httpResponse.getAllHeaders()) {
				headerBundle.putString("HEADER."
						+ header.getName().toUpperCase(), header.getValue());
			}

			byte[] arrayOfByte = new byte[1024];
			int length = 0;
			while ((length = localInputStream.read(arrayOfByte)) != -1) {
				// // System.out.println("zzz length: " + length);
				result += new String(arrayOfByte, 0, length);
				// ViewController.getInstance(context).loadingPercentage(
				// (int) (100 * (length / totalLength)));
			}
			return result;
		}
	}

	protected class RequestAsyncTask extends
			AsyncTask<Object, Integer, BaseEntity> {
		private OnDataRequestListener listener = null;
		private Bundle localParam = null;
		private int requestId = 0;

		public RequestAsyncTask() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected BaseEntity doInBackground(Object... param) {
			if (!(param[0] instanceof Request))
				return null;
			if (param[1] instanceof OnDataRequestListener)
				listener = (OnDataRequestListener) param[1];

			if (param[3] instanceof Integer)
				requestId = (Integer) param[3];

			if (param.length > 4 && param[4] instanceof Bundle)
				localParam = (Bundle) param[4];

			BaseEntity localBaseEntity = null;

			Log.d("Bello2",
					"prepare call api:" + ((Request) param[0]).toString() + ">"
							+ param[2].toString());
			// System.out.println("XXXXXXX who call?: " + listener);
			String printData = "";
			try {
				switch ((Request) param[0]) {
				case GET:
					if (param.length >= 6 && param[5] instanceof BaseEntity) {
						localBaseEntity = JsonParser.parseJSONtoEntity(
								printData = executeGet((String) param[2],
										localParam), (BaseEntity) param[5]);

					} else
						localBaseEntity = JsonParser.parseJSONtoEntity(
								printData = executeGet((String) param[2],
										localParam), null);
					break;
				case POST:
					if (param[4] instanceof Bundle)
						// printData = executePost((String) param[2],
						// ((Bundle) param[4])
						// .getString("POST_ENTITY", ""),
						// localParam);
						localBaseEntity = JsonParser
								.parseJSONtoEntity(
										printData = executePost(
												(String) param[2],
												((Bundle) param[4]).getString(
														"POST_ENTITY", ""),
												localParam), null);
					break;
				case PUT:
					if (param[4] instanceof Bundle)
						localBaseEntity = JsonParser
								.parseJSONtoEntity(
										printData = executePut(
												(String) param[2],
												((Bundle) param[4]).getString(
														"POST_ENTITY", ""),
												localParam), null);
					break;
				case DELETE:
					printData = executeDelete((String) param[2], localParam);
					break;
				}

				int length = printData.length();

				for (int i = 0; i < length; i += 1024) {
					if (i + 1024 < length)
						Log.d("Bello2 JSON OUTPUT",
								printData.substring(i, i + 1024));
					else
						Log.d("Bello2 JSON OUTPUT",
								printData.substring(i, length));
				}

				return localBaseEntity;
			} catch (Exception e) {
				e.printStackTrace();
				if (listener != null)
					listener.onConnectionError(e);
			}
			return null;
		}

		@Override
		protected void onPostExecute(BaseEntity entity) {
			if (localParam != null && headerBundle != null) {
				if (entity == null)
					entity = new BaseEntity();

				// if (localParam.getBoolean("SAVE_QUOTE", false))
				// PersistentData.getInstance().setQuoteId(
				// headerBundle.getString("HEADER.LOCATION"));

				if (localParam.getBoolean("SAVE_ORDER", false)) {
					entity.put("ORDER_ID",
							headerBundle.getString("HEADER.LOCATION"));
				}
				if (localParam.getBoolean("REFERAL_URL", false)) {
					entity.put("SHARE_URL",
							headerBundle.getString("HEADER.LOCATION"));
				}

				if (localParam.getBoolean("SAVE_LOGIN", false)) {
					entity.put("SAVE_LOGIN",
							headerBundle.getString("HEADER.LOCATION"));
				}

			}

			if (listener != null)
				listener.onAquireResult(entity, requestId, localParam);

			super.onPostExecute(entity);
		}
	}

	protected enum Request {
		GET, POST, PUT, DELETE;
	}

	protected enum LoadingDialogFlag {
		DIALOG_SHOW, DIALOG_DISMISS

	}

	/**
	 * abstract method to communication with server
	 * 
	 * @param context
	 * @param contentID
	 * @param listener
	 * @param position
	 * @param params
	 */
	public abstract void getData(Context context, int contentID,
			OnDataRequestListener listener, int position, Bundle params);

	/**
	 * Check Network connection
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetworkConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			return false;
		} else
			return true;
	}

}
