package com.citrineplexus.library.webservice;

import android.os.Bundle;

import com.citrineplexus.makandeal.merchant.entity.BaseEntity;

public interface OnDataRequestListener {
	public void onAquireResult(BaseEntity entity, int position, Bundle params);

	public void onConnectionError(Exception ex);
}
