package com.citrineplexus.library.imageloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.citrineplexus.makandeal.merchant.R;

public class ImageLoader {
	private static ImageLoader instance = null;
	MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;
	Handler handler = new Handler();// handler to display images in UI thread
	final int stub_id = R.drawable.img_preload;
	private boolean enableStub = true;

	private ImageLoader(Context context) {
		fileCache = new FileCache(context);
		executorService = Executors.newFixedThreadPool(5);
	}

	public static ImageLoader getInstance(Context context) {
		return instance = instance == null ? new ImageLoader(context)
				: instance;
	}

	private void CopyStream(InputStream is, OutputStream os) throws IOException {
		final int buffer_size = 1024;
		byte[] bytes = new byte[buffer_size];
		for (;;) {
			int count = is.read(bytes, 0, buffer_size);
			if (count == -1)
				break;
			os.write(bytes, 0, count);
		}
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			FileInputStream stream1 = new FileInputStream(f);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			FileInputStream stream2 = new FileInputStream(f);

			Bitmap bitmap = null;
			// todo: get view width
			// final int ScreenWidth = ViewController.getInstance(null)
			// .getScreenWidth();
			final int ScreenWidth = 480;

			if (o.outWidth > ScreenWidth) {
				int newBitmapHeight = ScreenWidth * o.outHeight / o.outWidth;
				int newBitmapWidth = ScreenWidth;
				Bitmap decodeBitmap = BitmapFactory.decodeStream(stream2, null,
						o2);
				bitmap = Bitmap.createScaledBitmap(decodeBitmap,
						newBitmapWidth, newBitmapHeight, true);
				decodeBitmap.recycle();
			} else {
				bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			}

			stream2.close();
			return bitmap;
		} catch (OutOfMemoryError localOutOfMemoryError) {
			localOutOfMemoryError.printStackTrace();
			this.memoryCache.clear();
			return null;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Bitmap getBitmap(String url, boolean isQRCode) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		if (!isQRCode) {
			// from web
			// try {
			URL imageUrl;
			try {
				imageUrl = new URL(url);

				HttpURLConnection conn = (HttpURLConnection) imageUrl
						.openConnection();
				conn.setConnectTimeout(30000);
				conn.setReadTimeout(30000);
				conn.setInstanceFollowRedirects(true);
				InputStream is = conn.getInputStream();
				OutputStream os = new FileOutputStream(f);
				CopyStream(is, os);
				os.close();
				conn.disconnect();
				return decodeFile(f);
			} catch (Throwable ex) {
				ex.printStackTrace();
				if (ex instanceof OutOfMemoryError)
					memoryCache.clear();
				if (ex instanceof IOException) {
					if (f.isFile())
						f.delete();
				}
				return null;
			}
		} else {
			byte[] decodedString = Base64.decode(url, Base64.DEFAULT);
			return BitmapFactory.decodeByteArray(decodedString, 0,
					decodedString.length);
		}
	}

	private void queuePhoto(String url, ImageView imageView, boolean isQRcode) {
		PhotoToLoad p = new PhotoToLoad(url, imageView, isQRcode);
		executorService.submit(new PhotosLoader(p));
	}

	public Bitmap getBitmap(String url) {
		Bitmap bitmap = memoryCache.get(url);
		if (bitmap == null) {
			bitmap = getBitmap(url, false);
			memoryCache.put(url, bitmap);
		}
		return bitmap;
	}

	public void DisplayImage(String url, ImageView imageView,
			ScaleType flagScaleType, boolean isQrCode) {
		imageViews.put(imageView, url);
		Bitmap bitmap = memoryCache.get(url);

		if (bitmap != null) {
			// float bmapWidth = bitmap.getWidth();
			// float bmapHeight = bitmap.getHeight();
			//
			// float wRatio = ViewController.getInstance(null).getScreenWidth()
			// / bmapWidth;
			// float hRatio = ViewController.getInstance(null).getScreenHeight()
			// / bmapHeight;
			//
			// float ratioMultiplier = wRatio;
			// // Untested conditional though I expect this might work for
			// // landscape
			// // mode
			// if (hRatio < wRatio) {
			// ratioMultiplier = hRatio;
			// }
			//
			// int newBmapWidth = (int) (bmapWidth * ratioMultiplier);
			// int newBmapHeight = (int) (bmapHeight * ratioMultiplier);
			imageView.setImageBitmap(bitmap);
			// imageView.setScaleType(ScaleType.FIT_XY);
			if (isQrCode)
				imageView.setScaleType(ScaleType.FIT_CENTER);
			else
				imageView.setScaleType(flagScaleType);
			imageView.setAnimation(null);
		} else {
			queuePhoto(url, imageView, isQrCode);
			imageView.setAnimation(null);
			imageView.setScaleType(ScaleType.FIT_CENTER);
			// imageView.setScaleType(flagScaleType);
			imageView.setImageResource(enableStub ? stub_id : 0);
			// imageView.setAnimation(ViewAnimation.rotateView());
		}
	}

	public void enableStub(boolean toggle) {
		enableStub = toggle;
	}

	public void clearCache() {
		this.memoryCache.clear();
		this.fileCache.clear();
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;
		boolean isQrCode = false;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if ((photoToLoad == null) || (photoToLoad.imageView == null)
					|| (imageViewReused(photoToLoad)))
				return;
			if (bitmap != null) {
				photoToLoad.imageView.setAnimation(null);
				photoToLoad.imageView.setImageBitmap(bitmap);
				if (!photoToLoad.isQRCode)
					photoToLoad.imageView.setScaleType(ScaleType.CENTER_CROP);
				else
					photoToLoad.imageView.setScaleType(ScaleType.FIT_CENTER);
				return;
			}
			try {
				// photoToLoad.imageView.setAnimation(ViewAnimation.rotateView());
				photoToLoad.imageView.setScaleType(ScaleType.FIT_CENTER);
				photoToLoad.imageView
						.setImageResource(enableStub ? stub_id : 0);
				return;
			} catch (Exception localException) {
				localException.printStackTrace();
			}
		}
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;
		public boolean isQRCode;

		public PhotoToLoad(String u, ImageView i, boolean isQRCode) {
			url = u;
			imageView = i;
			this.isQRCode = isQRCode;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			// try {
			if (imageViewReused(photoToLoad))
				return;
			Bitmap bmp = getBitmap(photoToLoad.url, photoToLoad.isQRCode);
			memoryCache.put(photoToLoad.url, bmp);
			if (imageViewReused(photoToLoad))
				return;
			BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
			handler.post(bd);
			// } catch (Throwable th) {
			// th.printStackTrace();
			// }
		}
	}
}