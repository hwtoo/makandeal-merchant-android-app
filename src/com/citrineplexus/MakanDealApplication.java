package com.citrineplexus;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.merchant.R;
import com.citrineplexus.makandeal.merchant.entity.BaseEntity;
import com.citrineplexus.makandeal.merchant.services.OAuthAuthorization;
import com.citrineplexus.makandeal.merchant.util.GlobalService;

public class MakanDealApplication extends Application implements
		OnDataRequestListener {
	private static MakanDealApplication _instance;

	private SharedPreferences prefs = null;
	private boolean isLogin = false;

	public static MakanDealApplication getInstance() {
		return _instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		initSingleton();
	}

	private void initSingleton() {
		_instance = this;
		getPreferences();
		OAuthAuthorization.getInstance();
		GlobalService.checkLive(this);
	}

	public boolean isUserLogin() {
		return isLogin;
	}

	public void userIsLogin() {
		isLogin = true;
	}

	public SharedPreferences getPreferences() {
		if (prefs == null)
			prefs = getSharedPreferences(
					getApplicationContext().getString(R.string.app_name),
					MODE_PRIVATE);
		return prefs;
	}

	private String VendorName = "", /* VendorLocation = "", */VendorID = "",
			VendorEmail = "", VendorBranchID = "", VendorBranchName = "";

	public void setValueFromServer(BaseEntity entity, String email) {
		VendorID = (String) entity.get("VENDOR_ID");
		VendorName = (String) entity.get("VENDOR_NAME");
		// VendorLocation = (String) entity.get("STREET");
		VendorBranchID = (String) entity.get("VENDOR_BRANCH_ID");
		VendorBranchName = (String) entity.get("BRANCH_NAME");
		VendorEmail = email;
	}

	public String getVendorEmail() {
		return this.VendorEmail;
	}

	public void storeValue() {
		if (prefs == null)
			getPreferences();
		prefs.edit().putString("VENDOR_ID", VendorID).commit();
		prefs.edit().putString("VENDOR_NAME", VendorName).commit();
		// prefs.edit().putString("STREET", VendorLocation).commit();
		prefs.edit().putString("EMAIL", VendorEmail).commit();
		prefs.edit().putString("VENDOR_BRANCH_ID", VendorBranchID).commit();
		prefs.edit().putString("BRANCH_NAME", VendorBranchName).commit();
	}

	public void removeAllValue() {
		if (prefs == null)
			getPreferences();
		prefs.edit().remove("VENDOR_ID").commit();
		prefs.edit().remove("VENDOR_NAME").commit();
		// prefs.edit().remove("STREET").commit();
		prefs.edit().remove("EMAIL").commit();
		prefs.edit().remove("VENDOR_BRANCH_ID").commit();
		prefs.edit().remove("BRANCH_NAME").commit();
	}

	public String getVendorName() {
		return VendorName;
	}

	public String getVendorBranchName() {
		return VendorBranchName;
	}

	public String getVendorBranchId() {
		return VendorBranchID;
	}

	public String getVendorID() {
		return VendorID;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	public void onConnectionError(Exception ex) {
		handler.sendEmptyMessage(0);
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			Toast.makeText(MakanDealApplication.this,
					getString(R.string.exceptionMsg), Toast.LENGTH_SHORT)
					.show();
		}
	};

}
